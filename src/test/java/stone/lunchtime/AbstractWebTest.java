// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime;

import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ObjectUtils;

import io.jsonwebtoken.Jwts;
import stone.lunchtime.dto.out.UserDtoOut;
import stone.lunchtime.entity.IngredientEntity;
import stone.lunchtime.entity.MealEntity;
import stone.lunchtime.entity.MenuEntity;
import stone.lunchtime.init.InitDataBase;
import stone.lunchtime.service.OrderService;
import stone.lunchtime.spring.security.filter.SecurityConstants;

/**
 * Mother class of all tests that uses Web (for controller so).
 */
public abstract class AbstractWebTest extends AbstractTest {

	@Autowired
	protected MockMvc mockMvc;

	/**
	 * Logs the user as a Lunch Lady. Creates a session.
	 *
	 * @return the ResultActions
	 * @throws Exception if an error occurred
	 */
	protected ResultActions logOut(ResultActions result) throws Exception {
		return this.mockMvc.perform(MockMvcRequestBuilders.get(SecurityConstants.AUTH_LOGOUT_URL)
				.header(SecurityConstants.TOKEN_HEADER, this.getJWT(result)));
	}

	/**
	 * Logs the user. Creates a session. Will assert the ok HttpStatus before
	 * returning the action.
	 *
	 * @param email an email
	 * @return the ResultActions
	 * @throws Exception if an error occurred
	 */
	protected ResultActions logMeIn(String email) throws Exception {
		var result = this.mockMvc.perform(MockMvcRequestBuilders.get(SecurityConstants.AUTH_LOGIN_URL)
				.param("email", email).param("password", InitDataBase.USER_DEFAULT_PWD));
		result.andExpect(MockMvcResultMatchers.status().isOk());
		return result;
	}

	/**
	 * Logs the user as a Lunch Lady. Creates a session. Will assert the ok
	 * HttpStatus before returning the action.
	 *
	 * @return the ResultActions
	 * @throws Exception if an error occurred
	 */
	protected ResultActions logMeInAsLunchLady() throws Exception {
		var result = this.logMeIn(InitDataBase.USER_EXISTING_EMAIL);
		result.andExpect(MockMvcResultMatchers.status().isOk());
		return result;
	}

	/**
	 * Gets the JWT inside the result.
	 *
	 * @param result where to find the JWT
	 * @return the JWT
	 */
	protected String getJWT(ResultActions result) {
		return result.andReturn().getResponse().getHeader(SecurityConstants.TOKEN_HEADER);
	}

	/**
	 * Gets the user dto inside the JWT.
	 *
	 * @param result where to find the JWT.
	 * @return the user dto inside the JWT
	 */
	protected UserDtoOut getUserInToken(ResultActions result) {
		var signingKey = this.env.getProperty("configuration.jwt.key",
				"-KaPdSgVkXp2s5v8y/B?E(H+MbQeThWmZq3t6w9z$C&F)J@NcRfUjXn2r5u7x!A%").getBytes();

		var token = this.getJWT(result);
		var parsedToken = Jwts.parserBuilder().setSigningKey(signingKey).build()
				.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""));

		var username = parsedToken.getBody().getSubject();

		if (!ObjectUtils.isEmpty(username)) {
			@SuppressWarnings("unchecked")
			Map<String, ?> userDto = (Map<String, ?>) parsedToken.getBody().get(SecurityConstants.TOKEN_USER);
			var userDtoOut = new UserDtoOut(userDto);
			return userDtoOut;
		}
		return null;
	}

	/**
	 * Gets the user's id inside the JWT.
	 *
	 * @param result where to find the JWT.
	 * @return the user's id inside the JWT
	 */
	protected Integer getUserIdInToken(ResultActions result) {
		var userDtoOut = this.getUserInToken(result);
		return userDtoOut.getId();
	}

	/**
	 * Logs as a random user that is not a Lunch Lady. Creates a session.
	 *
	 * @param idsToAvoid an id to avoid
	 * @return the ResultActions
	 * @throws Exception if an error occurred
	 */
	protected ResultActions logMeInAsNormalRandomUser(Integer... idsToAvoid) throws Exception {
		var userNotLunchLady = super.findASimpleUser(idsToAvoid);
		var result = this.logMeIn(userNotLunchLady.getEmail());
		result.andExpect(MockMvcResultMatchers.status().isOk());
		return result;
	}

	/**
	 * Gets a valid menu for this week
	 *
	 * @param forThisWeek if true menu will be for this week only
	 * @return a valid menu for this week
	 */
	protected MenuEntity getValidMenu(boolean forThisWeek) {
		var resu = forThisWeek ? this.menuDao.findAllAvailableForWeek(String.valueOf(OrderService.getCurrentWeekId()))
				: this.menuDao.findAllEnabled();
		if (resu.isPresent()) {
			var menus = resu.get();
			var random = new Random();
			return menus.get(random.nextInt(menus.size()));
		}
		throw new RuntimeException("No valid menu found!");
	}

	/**
	 * Gets a valid meal for this week
	 *
	 * @param forThisWeek if true meal will be for this week only
	 * @return a valid meal for this week
	 */
	protected MealEntity getValidMeal(boolean forThisWeek) {
		var resu = forThisWeek ? this.mealDao.findAllAvailableForWeek(String.valueOf(OrderService.getCurrentWeekId()))
				: this.mealDao.findAllEnabled();
		if (resu.isPresent()) {
			var menus = resu.get();
			var random = new Random();
			return menus.get(random.nextInt(menus.size()));
		}
		throw new RuntimeException("No valid menu found!");
	}

	/**
	 * Gets a valid ingredient
	 *
	 * @return a valid ingredient
	 */
	protected IngredientEntity getValidIngredient() {
		var resu = this.ingredientDao.findAllEnabled();
		if (resu.isPresent()) {
			var menus = resu.get();
			var random = new Random();
			return menus.get(random.nextInt(menus.size()));
		}
		throw new RuntimeException("No valid Ingredient found!");
	}

}
