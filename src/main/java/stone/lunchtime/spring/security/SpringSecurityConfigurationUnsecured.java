// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime.spring.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfigurationSource;

import stone.lunchtime.spring.security.filter.JwtAuthenticationFilter;
import stone.lunchtime.spring.security.filter.unsecured.JwtAuthorizationFilterUnsecured;

/**
 * Security configuration when you do not want it. <br>
 * This configuration can be used when the profile 'unsecured' is activated.
 * When used, you will never need to authenticate, and will always be a lunch
 * lady.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Profile("unsecured")
public class SpringSecurityConfigurationUnsecured extends AbstractSpringSecurityConfiguration {
	private static final Logger LOG = LogManager.getLogger();

	/**
	 * Global CORS configuration.
	 *
	 * @return global cors configuration for Spring Security.
	 */
	@Override
	@Bean
	protected CorsConfigurationSource corsConfigurationSource() {
		return super.corsConfigurationSource();
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager authenticationManager)
			throws Exception {
		SpringSecurityConfigurationUnsecured.LOG.debug("SpringSecurityConfigurationUnsecured - Apply rules");

		// Keep cors enable here, otherwise configuration of it is not applied
		http.csrf().disable().cors();

		// For H2
		http.headers().frameOptions().disable();

		// The JWT filter that will always say that you are lunchlady
		// We add the two filters and set session policy to Stateless
		http.authorizeRequests().and()
				.addFilterBefore(new JwtAuthenticationFilter(authenticationManager, this.env),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(new JwtAuthorizationFilterUnsecured(authenticationManager, this.env),
						UsernamePasswordAuthenticationFilter.class)
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		var exceptionHandling = http.authorizeRequests().and().exceptionHandling();
		exceptionHandling.authenticationEntryPoint(new RestAuthenticationEntryPoint());
		exceptionHandling.accessDeniedHandler(new AccesDeniedHandler());

		// For logout, simply send 200
		http.authorizeRequests().and().logout().clearAuthentication(true)
				.logoutSuccessHandler((pRequest, pResponse, pAuthentication) -> pResponse.setStatus(200));

		// No login, and no logout
		http.authorizeRequests().and().formLogin().disable().httpBasic().disable();

		http.authorizeRequests().anyRequest().permitAll();
		return http.build();
	}

}
