// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime.spring.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfigurationSource;

import stone.lunchtime.spring.security.filter.JwtAuthenticationFilter;
import stone.lunchtime.spring.security.filter.JwtAuthorizationFilter;

/**
 * Security configuration. <br>
 * This is default configuration. <br>
 * Use 'unsecured' profile if you do not want to be in secured mode. Handle ONLY
 * Spring Security
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@ConditionalOnMissingBean(SpringSecurityConfigurationUnsecured.class)
public class SpringSecurityConfigurationSecured extends AbstractSpringSecurityConfiguration {
	private static final Logger LOG = LogManager.getLogger();

	/**
	 * Global CORS configuration.
	 *
	 * @return global cors configuration for Spring Security.
	 */
	@Override
	@Bean
	protected CorsConfigurationSource corsConfigurationSource() {
		return super.corsConfigurationSource();
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager authenticationManager)
			throws Exception {
		SpringSecurityConfigurationSecured.LOG.debug("SpringSecurityConfigurationSecured - Apply rules");

		// Keep cors enable here, otherwise configuration of it is not applied
		http.csrf().disable().cors();

		http.authorizeRequests().antMatchers("/", // Root
				"/favicon.ico*", //
				"/csrf/**", //
				"/v3/api-docs/**", // Swagger
				"/v3/api-docs**", //
				"/configuration/**", //
				"/swagger-ui.html", //
				"/swagger-resources/**", //
				"/swagger*/**", //
				"/webjars/**", //
				"/h2/**", // h2 + frameOptions
				"/forgotpassword", // Lunchtime API
				"/constraint/findall", //
				"/constraint/find/**", //
				"/ingredient/find/**", //
				"/ingredient/findimg/**", //
				"/meal/find/**", //
				"/meal/findimg/**", //
				"/meal/findallavailableforweek/**", //
				"/meal/findallavailableforweekandday/**", //
				"/meal/findallavailablefortoday", //
				"/meal/findallavailableforthisweek", //
				"/menu/findallavailablefortoday", //
				"/menu/findallavailableforweekandday/**", //
				"/menu/findallavailableforweek/**", //
				"/menu/findallavailableforthisweek", //
				"/menu/find/**", //
				"/menu/findimg/**", //
				"/user/register", //
				"/img/**", //
				"/error", //
				"license.txt").permitAll();

		// For H2
		http.headers().frameOptions().disable();

		// The JWT filter
		// We add the two filters and set session policy to Stateless
		http.authorizeRequests().and()
				.addFilterBefore(new JwtAuthenticationFilter(authenticationManager, this.env),
						UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(new JwtAuthorizationFilter(authenticationManager, this.env),
						UsernamePasswordAuthenticationFilter.class)
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		var exceptionHandling = http.authorizeRequests().and().exceptionHandling();
		exceptionHandling.authenticationEntryPoint(new RestAuthenticationEntryPoint());
		exceptionHandling.accessDeniedHandler(new AccesDeniedHandler());

		// For logout, simply send 200
		http.authorizeRequests().and().logout().clearAuthentication(true)
				.logoutSuccessHandler((pRequest, pResponse, pAuthentication) -> pResponse.setStatus(200));

		// No login, and no logout
		http.authorizeRequests().and().formLogin().disable().httpBasic().disable();

		// Other constraint are handled at the controller level with annotations
		http.authorizeRequests().anyRequest().authenticated();
		return http.build();
	}

}
