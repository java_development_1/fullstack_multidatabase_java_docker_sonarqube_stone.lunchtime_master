// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime.spring.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import stone.lunchtime.spring.security.filter.SecurityConstants;

/**
 * Security configuration. <br>
 * This is default configuration. <br>
 * Use 'unsecured' profile if you do not want to be in secured mode. Handle ONLY
 * Spring Security
 */
abstract class AbstractSpringSecurityConfiguration {
	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	protected Environment env;
	@Autowired
	protected AuthenticationProvider customAuthenticationProvider;

	/**
	 * Global CORS configuration.
	 *
	 * @return global cors configuration for Spring Security.
	 */
	protected CorsConfigurationSource corsConfigurationSource() {
		AbstractSpringSecurityConfiguration.LOG
				.debug("AbstractSpringSecurityConfiguration - Loading CORS definition ...");
		var source = new UrlBasedCorsConfigurationSource();
		var config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOriginPattern("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");

		config.addExposedHeader("WWW-Authenticate");
		config.addExposedHeader("Access-Control-Allow-Origin");
		config.addExposedHeader("Access-Control-Allow-Headers");
		// In order to see the token for Angular
		config.addExposedHeader(SecurityConstants.TOKEN_HEADER);

		source.registerCorsConfiguration("/**", config);
		return source;
	}

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) {
		AbstractSpringSecurityConfiguration.LOG
				.debug("AbstractSpringSecurityConfiguration - Link with our Authentication provider");
		// Our Authentication Manager
		auth.authenticationProvider(this.customAuthenticationProvider);
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
			throws Exception {
		return authenticationConfiguration.getAuthenticationManager();
	}
}
