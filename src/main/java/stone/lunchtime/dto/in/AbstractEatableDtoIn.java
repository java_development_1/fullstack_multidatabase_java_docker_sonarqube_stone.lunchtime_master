// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime.dto.in;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JacksonException;

import io.swagger.v3.oas.annotations.media.Schema;
import stone.lunchtime.dto.AvailableForWeeksAndDays;
import stone.lunchtime.dto.WeekAndDay;
import stone.lunchtime.dto.out.AbstractDtoOut;
import stone.lunchtime.entity.AbstractEatableEntity;
import stone.lunchtime.service.exception.ParameterException;

/**
 * Dto with price and disponibilities. Package visibility
 *
 * @param <T> The targeted entity
 */
@Schema(description = "Represents a labeled element.", subTypes = { MealDtoIn.class, MenuDtoIn.class })
public abstract class AbstractEatableDtoIn<T extends AbstractEatableEntity> extends AbstractLabeledDtoIn<T> {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LogManager.getLogger();

	@Schema(description = "The price duty free for this element.", required = true, minimum = "0", maximum = "999")
	private BigDecimal priceDF;
	@Schema(description = "An json array of object that represents the week number and day number when this element is available.", nullable = true)
	private AvailableForWeeksAndDays availableForWeeksAndDays;

	/**
	 * Constructor of the object. <br>
	 */
	protected AbstractEatableDtoIn() {
		super();
	}

	/**
	 * Constructor of the object.<br>
	 *
	 * @param pEntity an entity
	 */
	protected AbstractEatableDtoIn(T pEntity) {
		super(pEntity);
		this.setPriceDF(pEntity.getPriceDF());
		try {
			this.setAvailableForWeeksAndDays(new AvailableForWeeksAndDays(pEntity.getAvailableForWeeksAndDays()));
		} catch (JacksonException exc) {
			AbstractEatableDtoIn.LOG.error("Error with weeks and day format", exc);
			this.setAvailableForWeeksAndDays(null);
		}
	}

	/**
	 * Gets the attribute value.
	 *
	 * @return the availableForWeeks value.
	 */
	public AvailableForWeeksAndDays getAvailableForWeeksAndDays() {
		return this.availableForWeeksAndDays;
	}

	/**
	 * Sets the attribute value.
	 *
	 * @param pAvailableForWeeks the new value for availableForWeeks attribute
	 */
	public void setAvailableForWeeksAndDays(AvailableForWeeksAndDays pAvailableForWeeks) {
		this.availableForWeeksAndDays = pAvailableForWeeks;
	}

	/**
	 * Gets the attribute value.
	 *
	 * @return the priceDF value.
	 */
	public BigDecimal getPriceDF() {
		return this.priceDF;
	}

	/**
	 * Sets the attribute value.
	 *
	 * @param pPriceDF the new value for priceDF attribute
	 */
	public void setPriceDF(BigDecimal pPriceDF) {
		this.priceDF = pPriceDF;
	}

	@Override
	@JsonIgnore
	public void validate() {
		if (this.getPriceDF() == null) {
			AbstractEatableDtoIn.LOG.error("validate - priceDF should not be null");
			throw new ParameterException("Il faut indiquer un prix HT", "priceDF");
		}
		if (this.getPriceDF() != null
				&& (this.getPriceDF().doubleValue() <= 0.001D || this.getPriceDF().doubleValue() > 999.99D)) {
			AbstractEatableDtoIn.LOG.error("validate - priceDF must be between ]0.001, 999.99]");
			throw new ParameterException("Prix HT invalid ! (doit être entre 0.001 et 999.99)", "priceDF");
		}
		if (this.getAvailableForWeeksAndDays() != null && !this.getAvailableForWeeksAndDays().isEmpty()) {
			for (WeekAndDay wd : this.getAvailableForWeeksAndDays().getValues()) {
				if (wd.getWeek() != null) {
					int w = wd.getWeek();
					if (w < 1 || w > 53) {
						AbstractEatableDtoIn.LOG.error("validate - week id must be between [1, 53], found {}", w);
						throw new ParameterException("Week Id invalide ! (doit être entre [1, 53])", "WeekId");

					}
					if (wd.getDay() != null) {
						int d = wd.getDay();
						if (d < 1 || d > 7) {
							AbstractEatableDtoIn.LOG.error("validate - day id must be between [1, 7], found {}", d);
							throw new ParameterException("Day Id invalide ! (doit être entre [1, 7])", "DayId");
						}
					}
				}
				if (wd.getWeek() == null && wd.getDay() != null) {
					AbstractEatableDtoIn.LOG.error("validate - week id cannot be null if day id exist");
					throw new ParameterException("Week Id invalide ! (un day id doit être rataché à un week id)",
							"DayId");
				}
			}
		}

	}

	@Override
	public String toString() {
		var sb = new StringBuilder();
		var parent = super.toString();
		parent = parent.substring(0, parent.length() - 1);
		sb.append(parent);
		sb.append(",priceDF=");
		sb.append(AbstractDtoOut.formatNumber(this.getPriceDF()));
		sb.append(",availableForWeeks=");
		if (this.getAvailableForWeeksAndDays() != null) {
			sb.append(this.getAvailableForWeeksAndDays());
		} else {
			sb.append("all");
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	@JsonIgnore
	protected T toEntity(T result) {
		super.toEntity(result);
		if (this.getAvailableForWeeksAndDays() != null) {
			try {
				result.setAvailableForWeeksAndDays(this.getAvailableForWeeksAndDays().toJson());
			} catch (JacksonException exc) {
				AbstractEatableDtoIn.LOG.error("Error in week dans day format!", exc);
			}
		} else {
			result.setAvailableForWeeksAndDays(null);
		}
		result.setPriceDF(this.getPriceDF());
		return result;
	}
}
