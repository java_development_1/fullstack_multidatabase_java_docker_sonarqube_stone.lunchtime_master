package stone.lunchtime.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Filter that will be used by Log4J in order to write the IP of the user.
 *
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class Log4J2Filter implements Filter {
	private static final Logger LOG = LogManager.getLogger();

	private static final String IP_ADDR = "ipAddress";

	private boolean activate;

	/**
	 * Constructor of the object.
	 */
	public Log4J2Filter() {
		super();
		this.activate = true;
	}

	@Override
	public void init(FilterConfig aFilterConfig) throws ServletException {
		if (aFilterConfig != null) {
			var activateStr = aFilterConfig.getInitParameter("activate");
			if (activateStr != null) {
				this.activate = "true".equalsIgnoreCase(activateStr);
			}
		}

		if (!this.activate) {
			Log4J2Filter.LOG.warn("Log4J2Filter is deactivated!");
		} else {
			Log4J2Filter.LOG.info("Log4J2Filter is activated!");
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		if (servletRequest instanceof HttpServletRequest) {
			var request = (HttpServletRequest) servletRequest;
			if (this.activate) {
				ThreadContext.put(Log4J2Filter.IP_ADDR, request.getRemoteAddr());
			}
		}
		// Whatever
		try {
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			ThreadContext.remove(Log4J2Filter.IP_ADDR);
		}
	}

	@Override
	public void destroy() {
		// Does nothing by default
	}

}
