// -#--------------------------------------
// -# ©Copyright Ferret Renaud 2019 -
// -# Email: admin@ferretrenaud.fr -
// -# All Rights Reserved. -
// -#--------------------------------------

package stone.lunchtime.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stone.lunchtime.dao.IMealDao;
import stone.lunchtime.dao.IMenuDao;
import stone.lunchtime.dto.in.MenuDtoIn;
import stone.lunchtime.entity.EntityStatus;
import stone.lunchtime.entity.ImageEntity;
import stone.lunchtime.entity.MealEntity;
import stone.lunchtime.entity.MenuEntity;
import stone.lunchtime.service.exception.EntityNotFoundException;
import stone.lunchtime.service.exception.ParameterException;

/**
 * Handle menu.
 */
@Service
public class MenuService extends AbstractServiceForEatableEntity<MenuEntity> {
	private static final Logger LOG = LogManager.getLogger();

	@Autowired
	private IMealDao mealDao;
	@Autowired
	private IMenuDao menuDao;

	/**
	 * Will add a menu into the data base.
	 *
	 * @param pDto information to be added.
	 * @return the entity added
	 * @throws ParameterException if parameter is invalid
	 */
	@Transactional(rollbackFor = Exception.class)
	public MenuEntity add(MenuDtoIn pDto) {
		MenuService.LOG.debug("add - {}", pDto);
		if (pDto == null) {
			MenuService.LOG.error("add  - DTO is null");
			throw new ParameterException("DTO est null !", "pDto");
		}
		pDto.validate();
		var menuInsert = pDto.toEntity();
		menuInsert.setStatus(EntityStatus.ENABLED);

		this.handleMeals(menuInsert, pDto.getMealIds());

		super.handleImage(menuInsert, pDto);

		var resultSave = this.menuDao.save(menuInsert);
		MenuService.LOG.info("add - OK with new id={}", resultSave.getId());
		return resultSave;
	}

	/**
	 * Updates entity. <br>
	 *
	 * This method does not change status.
	 *
	 * @param pIdToUpdate an entity id. The one that needs update.
	 * @param pNewDto     the new values for this entity
	 * @return the updated entity
	 * @throws EntityNotFoundException if entity not found
	 * @throws ParameterException      if parameter is invalid
	 */
	@Transactional(rollbackFor = Exception.class)
	public MenuEntity update(Integer pIdToUpdate, MenuDtoIn pNewDto) throws EntityNotFoundException {
		var entityInDateBase = super.beginUpdate(pIdToUpdate, pNewDto);

		this.handleMeals(entityInDateBase, pNewDto.getMealIds());

		var resultUpdate = this.menuDao.save(entityInDateBase);
		MenuService.LOG.info("update - OK in {}", this.getClass().getSimpleName());
		return resultUpdate;

	}

	/**
	 * Finds all menu available for the given week.
	 *
	 * @param pWeek a week id [1, 53]
	 * @return all meal available for this week
	 * @throws ParameterException if parameter is invalid
	 */
	@Transactional(readOnly = true)
	public List<MenuEntity> findAllAvailableForWeek(Integer pWeek) {
		MenuService.LOG.debug("findAllAvailableForWeek - {}", pWeek);
		if (pWeek == null || pWeek.intValue() < 1 || pWeek.intValue() > 53) {
			MenuService.LOG.error("findAllAvailableForWeek  - pWeek is null or not in [1, 53]");
			throw new ParameterException(
					"Le numero de semaine ne peut pas être null et doit être compris entre [1, 53] !");
		}
		var opResult = this.menuDao.findAllAvailableForWeek(pWeek.toString());
		if (opResult.isPresent()) {
			var result = opResult.get();
			MenuService.LOG.debug("findAllAvailableForWeek - found {} values for week {}", result.size(), pWeek);
			return result;
		}
		MenuService.LOG.debug("findAllAvailableForWeek - found NO value for week {}", pWeek);
		return Collections.emptyList();
	}

	/**
	 * Finds all menu available for the given week and day.
	 *
	 * @param pWeek a week id [1, 53]
	 * @param pDay  a day if [1, 7]
	 * @return all meal available for this week and day
	 * @throws ParameterException if parameter is invalid
	 */
	@Transactional(readOnly = true)
	public List<MenuEntity> findAllAvailableForWeekAndDay(Integer pWeek, Integer pDay) {
		MenuService.LOG.debug("findAllAvailableForWeekAndDay - {}", pWeek);
		if (pWeek == null || pWeek.intValue() < 1 || pWeek.intValue() > 53) {
			MenuService.LOG.error("findAllAvailableForWeekAndDay  - pWeek is null or not in [1, 53]");
			throw new ParameterException(
					"Le numero de semaine ne peut pas être null et doit être compris entre [1, 53] !");
		}
		if (pDay == null || pDay.intValue() < 1 || pDay.intValue() > 7) {
			MenuService.LOG.error("findAllAvailableForWeekAndDay  - pDay is null or not in [1, 7]");
			throw new ParameterException("Le numero de jour ne peut pas être null et doit être compris entre [1, 7] !");
		}
		var opResult = this.menuDao.findAllAvailableForWeekAndDay(pWeek.toString(), pDay.toString());
		if (opResult.isPresent()) {
			var result = opResult.get();
			MenuService.LOG.debug("findAllAvailableForWeekAndDay - found {} values for week {} and day {}",
					result.size(), pWeek, pDay);
			return result;
		}
		MenuService.LOG.debug("findAllAvailableForWeekAndDay - found NO value for week {} and day {}", pWeek, pDay);
		return Collections.emptyList();
	}

	/**
	 * Handles the join between menu and meal.
	 *
	 * @param pMenuEntity a menu
	 * @param pMealIds    a list of meal's id
	 */
	private void handleMeals(MenuEntity pMenuEntity, List<Integer> pMealIds) {
		if (pMealIds != null && !pMealIds.isEmpty()) {
			List<MealEntity> meals = new ArrayList<>();
			for (Integer mealId : pMealIds) {
				var oldMeal = this.mealDao.findById(mealId);
				if (oldMeal.isPresent()) {
					MenuService.LOG.trace("handleMeals - adding meal with id {}", mealId);
					meals.add(oldMeal.get());
				} else {
					MenuService.LOG.warn("handleMeals - cannot add meal with id {} because not found", mealId);
				}
			}
			pMenuEntity.setMeals(meals);
		} else {
			MenuService.LOG.warn("handleMeals (no meal)");
			pMenuEntity.setMeals(null);
		}
	}

	@Override
	protected CrudRepository<MenuEntity, Integer> getTargetedDao() {
		return this.menuDao;
	}

	@Override
	protected ImageEntity getDefault() {
		return super.getImageService().saveIfNotInDataBase(DefaultImages.MENU_DEFAULT_IMG);
	}
}
